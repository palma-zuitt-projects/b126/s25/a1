/*

Activity:

In postman, create a new collection named s25-a1 and add the following requests to that collection:

1. GET request to jsonplaceholder's todo resource
2.GET Specific request todo id 199 to jsonplaceholders todo resource
3. POST request to jsonplacehorlders' todo resource
4. PUT request for todo id 123 to jsonplaceholders todo resource.
5. DELETE erequest for todo id 7 todo resource

Make sure to test each request to see if its working, and if it is working, make sure to properly save the request.

Once complete, export the collection to s25/a1 push to Gitlab, and paste to the appropriate Boodle Session.
*/


const http = require("http");

const server = http.createServer(function (request, response){

	if(request.url === "/items" && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Item retrieved from data base')
	}else if(request.url === "/items/1234567890" && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Item (specific name) added')
	}else if(request.url === "/items/1234567890" && request.method === "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Item (specific name) updated')
	}else if(request.url === "/items/1234567890" && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Item (specific name) deleted')

	}else if(request.url === "/items" && request.method === "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Item added to data base')
	}
})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)